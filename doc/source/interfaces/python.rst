Python interface
==================

The Python interface is a binding from the C++ interface. This means that
it will be very fast, with similar performance to the C++. It depends on
the pybind11 C++ library to be built.

To use it you will need to build the Python Dynamic Library (.dll on Windows
and .so on Unix). To do it, you will need to run the following commands:

>>> sudo apt install cmake build-essential qt5-default libqt5serialport5-dev libqt5serialport5 python3-dev
>>> cd quark
>>> git submodule init
>>> git submodule update
>>> mkdir build
>>> cd build
>>> cmake ..
>>> make

Then, after you build it without errors, you need to copy the files

- interfaces/python/quark.py
- build/interfaces/python/quark_python.cpython...so
  (or .dll in windows)

to your project folder.


.. note::

    I know that it is annoying to build things. Some may also argue about a
    native Python implementation since Quark communication is agnostic.
    However, in my personal benchmarks, the native Python implementation
    versus the C++ binding showed itself to be more than 20x slower.


And here it is the API documentation. Basically you will use the Quark class.

.. autoclass:: quark.Quark
    :members:
    :undoc-members:
    :show-inheritance:




