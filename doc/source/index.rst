Welcome to Quark documentation
================================

Quark is a firmware for STM32 micro-controllers to make them operate as data
acquisition boards. It was designed to be easy to use for control applications:

- It is controlled by the computer through a USB 2.0 cable
- In theory, rates of until 4 kHz can be obtained
- The following hardware is supported:

    - Read up to 2 encoders (count and pulse duration measurements)
    - 4x A/D of 12 bits, reading voltages from 0 to 3.3 V
    - 4x PWM channels and it is possible to set the PWM frequency
    - 4x GPIO output (push-pull and open-drain modes)
    - 4x GPIO input
    - 1x I2C (drivers for devices coming soon)
    - 3x OneWire (drivers for devices coming soon)

The firmware is written in C++ but you do not need to worry. The board can be
controlled from:

- Python 3 (pure Python implementation)
- MATLAB/Simulink (support coming soon)
- Any language/computer that supports reading/writing UART ports


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   firmware/index
   interfaces/index
   contributing/index
   



Index and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
