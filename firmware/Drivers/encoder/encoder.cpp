#include "encoder.h"

namespace encoder {

Encoder::Encoder()
{
}

void Encoder::begin(uint8_t number)
{
	enabled = false;
	actual_quadrature = Quadrature::NONE;
	set_cw_is_positive();
	actual_code = read_gpio();
	pulses_count = 0;
	duration_of_new_pulse_us = timeout_us;
	duration_of_last_pulse_us = timeout_us;

	chA.Pull = GPIO_PULLUP;
	chB.Pull = GPIO_PULLUP;
	chA.Mode = GPIO_MODE_INPUT;
	chB.Mode = GPIO_MODE_INPUT;

	switch(number) {
		default:
			break;

		case ENCODER_1:
			chA.Pin = ENC1_CHA_Pin;
			chA_pin_port = ENC1_CHA_GPIO_Port;
			chA_irq_number = ENC1_CHA_EXTI_IRQ;

			chB.Pin = ENC1_CHB_Pin;
			chB_pin_port = ENC1_CHB_GPIO_Port;
			chB_irq_number = ENC1_CHB_EXTI_IRQ;
			break;

		case ENCODER_2:
			chA.Pin = ENC2_CHA_Pin;
			chA_pin_port = ENC2_CHA_GPIO_Port;
			chA_irq_number = ENC2_CHA_EXTI_IRQ;

			chB.Pin = ENC2_CHB_Pin;
			chB_pin_port = ENC2_CHB_GPIO_Port;
			chB_irq_number = ENC2_CHB_EXTI_IRQ;
			break;
	}

	// Moving average filter initialization
	filter.begin(filter_size);
	filter.reset(timeout_us);
}

uint8_t Encoder::read_gpio_chA()
{
	return (uint8_t) HAL_GPIO_ReadPin(chA_pin_port, chA.Pin);
}

uint8_t Encoder::read_gpio_chB()
{
	return (uint8_t) HAL_GPIO_ReadPin(chB_pin_port, chB.Pin);
}

GrayCode Encoder::read_gpio()
{
	uint8_t chA_state = read_gpio_chA();
	uint8_t chB_state = read_gpio_chB();
	return (GrayCode) ((chB_state << 1) | chA_state);
}

void Encoder::update_state(GrayCode new_code, int32_t timer_value_us)
{
	switch (actual_code) {
	case GrayCode::GRAY_0:
		switch (new_code) {

		// FORWARD
		case GrayCode::GRAY_1:
			pulses_count = pulses_count + cw_increment;
			 // detected rising edge on chA
			update_time(timer_value_us);
			break;

		// REVERSE
		case GrayCode::GRAY_3:
			pulses_count = pulses_count - cw_increment;
			break;

		// INVALID or STOPPED
		default:
			break;
		}
		break;

	case GrayCode::GRAY_1:
		switch (new_code) {

		// FORWARD
		case GrayCode::GRAY_2:
			pulses_count = pulses_count + cw_increment;
			break;

		// REVERSE
		case GrayCode::GRAY_0:
			pulses_count = pulses_count - cw_increment;
			break;

		// INVALID or STOPPED
		default:
			break;
		}
		break;

	case GrayCode::GRAY_2:
		switch (new_code) {

		// FORWARD
		case GrayCode::GRAY_3:
			pulses_count = pulses_count + cw_increment;
			break;

		// REVERSE
		case GrayCode::GRAY_1:
			pulses_count = pulses_count - cw_increment;
			break;

		// INVALID or STOPPED
		default:
			break;
		}
		break;

	case GrayCode::GRAY_3:
		switch (new_code) {

		// FORWARD
		case GrayCode::GRAY_0:
			pulses_count = pulses_count + cw_increment;
			break;

		// REVERSE
		case GrayCode::GRAY_2:
			pulses_count = pulses_count - cw_increment;
			 // detected rising edge on chA
			update_time(timer_value_us);
			break;

		// INVALID or STOPPED
		default:
			break;
		}
		break;
	}

	actual_code = new_code;
}

void Encoder::verify_timeout()
{
	if (duration_of_new_pulse_us > timeout_us)
	{
		filter.add(timeout_us);
		duration_of_last_pulse_us = filter.get_average();
		duration_of_new_pulse_us = 0;
	}
}

void Encoder::update_count(int32_t timer_value_us)
{
	GrayCode new_code = read_gpio();
	update_state(new_code, timer_value_us);
}

void Encoder::update_time(int32_t timer_value_us)
{
	int32_t new_measurement = timer_value_us + duration_of_new_pulse_us;
	if (new_measurement < 0)
	{
		new_measurement = new_measurement + ENC_TIMER_OVERFLOW_US;
	}

	// Limit the variation
	float new_value = new_measurement;
	float old_value = duration_of_last_pulse_us;
	float variation = (new_value - old_value) / old_value;
	if (variation > max_rate)
	{
		variation = max_rate;
		new_value = (1 + variation) * old_value;
	}

	else if (variation < -max_rate)
	{
		variation = -max_rate;
		new_value = (1 + variation) * old_value;
	}

//	new_value = (1 + variation) * old_value;
	new_measurement = (int32_t) new_value;

	filter.add(new_measurement);
	duration_of_last_pulse_us = filter.get_average();
	duration_of_new_pulse_us = -timer_value_us;
}

void Encoder::update_on_overflow()
{
	duration_of_new_pulse_us = duration_of_new_pulse_us + ENC_TIMER_OVERFLOW_US;
	verify_timeout();
}

void Encoder::set_cw_is_positive()
{
	cw_increment = 1;
}

void Encoder::set_ccw_is_positive()
{
	cw_increment = -1;
}

void Encoder::set_timeout(uint32_t new_timeout)
{
	timeout_us = new_timeout;
}

uint32_t Encoder::get_timeout()
{
	return timeout_us;
}

void Encoder::set_filter_size(uint8_t size)
{
    filter_size = size;
    filter.begin(filter_size);
    filter.reset(timeout_us);
}

uint8_t Encoder::get_filter_size()
{
    return filter_size;
}

void Encoder::set_max_rate(uint8_t new_rate)
{
    max_rate = static_cast<float>(new_rate) / 100.0f;
}

uint8_t Encoder::get_max_rate()
{
    return max_rate;
}

uint8_t Encoder::get_number()
{
	return number;
}

bool Encoder::is_enabled()
{
	return enabled;
}

void Encoder::enable_gpio_irq()
{
	// Enable pin interrupt if it is not enabled
	if (HAL_NVIC_GetActive(chA_irq_number) == 0)
	{
		HAL_NVIC_SetPriority(chA_irq_number, ENCODER_PREEMPT_PRIORITY, ENCODER_SUB_PRIORITY);
		HAL_NVIC_EnableIRQ(chA_irq_number);
	}

	if (HAL_NVIC_GetActive(chB_irq_number) == 0)
	{
		HAL_NVIC_SetPriority(chB_irq_number, ENCODER_PREEMPT_PRIORITY, ENCODER_SUB_PRIORITY);
		HAL_NVIC_EnableIRQ(chB_irq_number);
	}
}

void Encoder::update_gpio_modes(Quadrature new_quadrature)
{
	HAL_GPIO_DeInit(chA_pin_port, chA.Pin);
	HAL_GPIO_DeInit(chB_pin_port, chB.Pin);

	switch (new_quadrature) {
	case Quadrature::NONE:
		chA.Mode = GPIO_MODE_INPUT;
		chB.Mode = GPIO_MODE_INPUT;
		break;

	case Quadrature::X1:
		chA.Mode = GPIO_MODE_IT_RISING;
		chB.Mode = GPIO_MODE_INPUT;
		break;

	case Quadrature::X2:
		chA.Mode = GPIO_MODE_IT_RISING_FALLING;
		chB.Mode = GPIO_MODE_INPUT;
		break;

	case Quadrature::X4:
		chA.Mode = GPIO_MODE_IT_RISING_FALLING;
		chB.Mode = GPIO_MODE_IT_RISING_FALLING;
		break;
	}

	HAL_GPIO_Init(chA_pin_port, &chA);
	HAL_GPIO_Init(chB_pin_port, &chB);
}

void Encoder::enable()
{
	update_gpio_modes(actual_quadrature);
	enable_gpio_irq();
	enabled = true;
}

void Encoder::disable()
{
	update_gpio_modes(Quadrature::NONE);
	enabled = false;
}

void Encoder::set_quadrature(Quadrature new_quadrature)
{
	actual_quadrature = new_quadrature;
}

void Encoder::reset_couting()
{
	pulses_count = 0;
}

void Encoder::reset_time_measurement()
{
	duration_of_new_pulse_us = timeout_us;
	duration_of_last_pulse_us = timeout_us;

	// Also reset the filter
	filter.reset(timeout_us);
}

int32_t Encoder::get_pulses_count()
{
	return pulses_count;
}

uint32_t Encoder::get_duration_of_last_pulse()
{
	return duration_of_last_pulse_us;
}

void Encoder::isr_pin_change(int32_t timer_count_us)
{
    event_t new_event;
    new_event.type = EVENT_PIN_CHANGE;
    new_event.timer_count_us = timer_count_us;
    new_event.pin_state = read_gpio();
    queue.add(new_event);
}

void Encoder::isr_timer_overflow()
{
    event_t new_event;
    new_event.type = EVENT_TIMER_OVERFLOW;
    queue.add(new_event);
}

bool Encoder::callback()
{
    bool events_were_processed = false;

    // If the queue is not empty, then we have events to process
    if (!queue.isEmpty())
    {
        event_t event;
        while (queue.pull(&event))
        {
            // Handle a pin change
            if (event.type == EVENT_PIN_CHANGE)
            {
                update_state(event.pin_state, event.timer_count_us);
            }

            // Handle a timer overflow
            else // if (event.type == EVENT_TIMER_OVERFLOW)
            {
                update_on_overflow();
            }
        }

        events_were_processed = true;
    }

    return events_were_processed;
}


}; // namespace encoder
