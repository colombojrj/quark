#ifndef MANAGER_MEMORY_TABLE_H_
#define MANAGER_MEMORY_TABLE_H_

/**
  ******************************************************************************
  * @file    memory_table.h
  * @author  José Roberto Colombo Junior
  * @brief   Header file of the memory table
  ******************************************************************************
  * @attention
  *
  * This is free software licensed under GPL.
  *
  ******************************************************************************
  */

#ifdef __cplusplus
	extern "C" {
#endif

#include <stdint.h>
#include "Config/board.h"

/**
 * @defgroup memory_table Memory Table
 *
 * The board is controlled by a memory table, which holds all
 * off its configurations
 *
 */

/**@{*/

/**
 * @brief Defines the memory_table namespace
 */
namespace memory_table {
	/**
	 * @brief Registers of the memory table
	 */
	enum class Register : uint8_t {
		GPIO_INPUT_STATE = 		0x00,					///< Logic state of the input pins (type uint8_t)
		GPIO_INPUT_PULL = 		0x01,					///< Pull resistor of the input pins (type uint8_t)
		GPIO_OUTPUT_STATE = 	0x02,					///< Logic state of the output pins (type uint8_t)
		GPIO_OUTPUT_PULL = 		0x03,					///< Pull resistors of the output pins (type uint8_t)
		GPIO_OUTPUT_MODE =		0x04,					///< Driver mode of the output pins (type uint8_t)
		ADC_CONFIG =			0x05,					///< ADC configuration (type uint8_t)
		ADC_FILTER_FREQ = 		0x06,					///< Frequency of the low-pass filter for the ADC (type uint16_t)
		ADC_DATA_CH1 =			0x08,					///< Raw data from CH1 of the ADC
		ADC_DATA_CH2 =			0x0A,					///< Raw data from CH2 of the ADC
		ADC_DATA_CH3 =			0x0C,					///< Raw data from CH3 of the ADC
		ADC_DATA_CH4 =			0x0E,					///< Raw data from CH4 of the ADC
		PWM_FREQUENCY =			0x10,					///< Frequency of the PWM
		PWM_DUTY_CH1 =			0x14,					///< Duty cycle for CH1 of the PWM
		PWM_DUTY_CH2 =			0x18,					///< Duty cycle for CH2 of the PWM
		PWM_DUTY_CH3 =			0x1C,					///< Duty cycle for CH3 of the PWM
		PWM_DUTY_CH4 =			0x20,					///< Duty cycle for CH4 of the PWM
		ENC_1_COUNT =           0x24,
		ENC_1_TIME =            0x28,
		ENC_2_COUNT =           0x2C,
		ENC_2_TIME =            0x30,
		ENC_1_CONFIG =          0x34,					///< Encoder 1 configuration
		ENC_2_CONFIG =          0x35,					///< Encoder 2 configuration
		ENC_1_TIMEOUT_MS =      0x36,
		ENC_2_TIMEOUT_MS =      0x37,
        ENC_1_FILTER_SIZE =     0x38,
        ENC_2_FILTER_SIZE =     0x39,
        ENC_1_REL_MAX_RATE =    0x3A,
        ENC_2_REL_MAX_RATE =    0x3B,
		VERSION_MAJOR =			0x7C,
		VERSION_MINOR = 		0x7D,
		VERSION_PATCH =			0x7E,
		WHO_AM_I =				0x7F,
	};

	/**
	 * @brief Initial values of the Register of the memory table
	 */
	enum class InitialValue : uint32_t {
		GPIO_INPUT_STATE = 0x0F,						///< Initial value of the Register::GPIO_INPUT_STATE
		GPIO_INPUT_PULL = 0x55,							///< Initial value of the Register::GPIO_INPUT_PULL
		GPIO_OUTPUT_STATE = 0x00,						///< Initial value of the Register::GPIO_OUTPUT_STATE
		GPIO_OUTPUT_PULL = 0x00,						///< Initial value of the Register::GPIO_OUTPUT_PULL
		GPIO_OUTPUT_MODE = 0x00,						///< Initial value of the Register::GPIO_OUTPUT_MODE
		ADC_CONFIG = 0x00,								///< Initial value of the Register::ADC_CONFIG
		ADC_FILTER_FREQ = 0x00,							///< Initial value of the Register::ADC_FILTER_FREQ
		PWM_FREQUENCY = 10000,							///< Initial value of the Register::PWM_FREQUENCY
		PWM_DUTY_CH1 = 0x00,							///< Initial value of the Register::PWM_DUTY_CH1
		PWM_DUTY_CH2 = 0x00,							///< Initial value of the Register::PWM_DUTY_CH2
		PWM_DUTY_CH3 = 0x00,							///< Initial value of the Register::PWM_DUTY_CH3
		PWM_DUTY_CH4 = 0x00,							///< Initial value of the Register::PWM_DUTY_CH4
		ENC_1_CONFIG = 0x04,						    ///< Initial value of the Register::ENCODER_1_CONFIG
		ENC_2_CONFIG = 0x04,						    ///< Initial value of the Register::ENCODER_2_CONFIG
		ENC_1_TIME = ENC_DEFAULT_TIMEOUT_US,            ///< Initial value of the Register::ENC_1_TIME
		ENC_2_TIME = ENC_DEFAULT_TIMEOUT_US,            ///< Initial value of the Register::ENC_2_TIME
		ENC_1_TIMEOUT_MS = ENC_DEFAULT_TIMEOUT_MS,
		ENC_2_TIMEOUT_MS = ENC_DEFAULT_TIMEOUT_MS,
		ENC_1_FILTER_SIZE = 4,
		ENC_2_FILTER_SIZE = 4,
		ENC_1_MAX_RATE = 10,
		ENC_2_MAX_RATE = 10,
	};

	/**
	 * @brief Possible configurations of the GPIO pull-resistors
	 */
	enum class GPIO_PULL : uint8_t {
		NONE = 0b00,									///< No pull resistor
		UP = 0b01,										///< Use a pull-up resistor
		DOWN = 0b10,									///< Use a pull-down resistor
	};

	/**
	 * @brief Available output modes
	 */
	enum class GPIO_OUTPUT_MODE : uint8_t {
		PUSH_PULL = 0,									///< Push-pull (forces LOW or HIGH values)
		OPEN_DRAIN = 1,									///< Open-drain (forces only LOW values, HIGH is achieved by the use of a pull-up resistor)
	};

	/**
	 * @brief Positions of the Register::GPIO_INPUT_PULL
	 */
	enum class POSITIONS_GPIO_INPUT_PULL : uint8_t {
		CH1 = 0,										///< CH1 position
		CH2 = 2,										///< CH2 position
		CH3 = 4,										///< CH3 position
		CH4 = 6,										///< CH4 position
		CH1_MASK = 0b11 << CH1,							///< CH1 mask
		CH2_MASK = 0b11 << CH2,							///< CH2 mask
		CH3_MASK = 0b11 << CH3,							///< CH3 mask
		CH4_MASK = 0b11 << CH4,							///< CH4 mask
	};

	/**
	 * @brief Positions of the Register::GPIO_INPUT_STATE
	 */
	enum class POSITIONS_GPIO_INPUT_STATE : uint8_t {
		CH1 = 0,										///< CH1 position
		CH2 = 1,										///< CH2 position
		CH3 = 2,										///< CH3 position
		CH4 = 3,										///< CH4 position
		CH1_MASK = (1 << CH1),							///< CH1 mask
		CH2_MASK = (1 << CH2),							///< CH2 mask
		CH3_MASK = (1 << CH3),							///< CH3 mask
		CH4_MASK = (1 << CH4),							///< CH4 mask
	};

	/**
	 * @brief Positions of the Register::GPIO_OUTPUT_STATE
	 */
	enum class POSITIONS_GPIO_OUTPUT_STATE : uint8_t {
		CH1 = 0,										///< CH1 position
		CH2 = 1,										///< CH2 position
		CH3 = 2,										///< CH3 position
		CH4 = 3,										///< CH4 position
		CH1_MASK = (1 << CH1),							///< CH1 mask
		CH2_MASK = (1 << CH2),							///< CH2 mask
		CH3_MASK = (1 << CH3),							///< CH3 mask
		CH4_MASK = (1 << CH4),							///< CH4 mask
	};

	/**
	 * @brief Positions of the Register::GPIO_OUTPUT_PULL
	 */
	enum class POSITIONS_GPIO_OUTPUT_PULL : uint8_t {
		CH1 = 0,										///< CH1 position
		CH2 = 2,										///< CH2 position
		CH3 = 4,										///< CH3 position
		CH4 = 6,										///< CH4 position
		CH1_MASK = 0b11 << CH1,							///< CH1 mask
		CH2_MASK = 0b11 << CH2,							///< CH2 mask
		CH3_MASK = 0b11 << CH3,							///< CH3 mask
		CH4_MASK = 0b11 << CH4,							///< CH4 mask
	};

	/**
	 * @brief Positions of the Register::GPIO_OUTPUT_MODE
	 */
	enum class POSITIONS_GPIO_OUTPUT_MODE : uint8_t {
		CH1 = 0,										///< CH1 position
		CH2 = 1,										///< CH2 position
		CH3 = 2,										///< CH3 position
		CH4 = 3,										///< CH4 position
		CH1_MASK = (1 << CH1),							///< CH1 mask
		CH2_MASK = (1 << CH2),							///< CH2 mask
		CH3_MASK = (1 << CH3),							///< CH3 mask
		CH4_MASK = (1 << CH4),							///< CH4 mask
	};

	/**
	 * Positions of the Register::ADC_CONFIG
	 */
	enum class POSITIONS_ADC_CONFIG : uint8_t {
		ENABLE = 0,										///< Enable position
		ENABLE_MASK = (1 << ENABLE),					///< Enable mask
	};

	/**
	 * Positions of the Registers ENCODER_1_CONFIG
	 */
	enum class POSITIONS_ENCODER_CONFIG : uint8_t {
		QUADRATURE = 0,									///< Quadrature bits
		DIRECTION = 2,									///< Used to set if counts +1 in CW or CCW rotation
		RESET_COUNTING = 3,								///< Reset the pulses count
		ENABLE = 4,										///< Enable or disable
		RESET_TIME = 5,									///< Reset the time measurement
		QUADRATURE_MASK = (0b11 << QUADRATURE),			///< Quadrature mask
		DIRECTION_MASK = (1 << DIRECTION),				///< Direction mask
		RESET_COUNTING_MASK = (1 << RESET_COUNTING),	///< Reset counting mask
		ENABLE_MASK = (1 << ENABLE),					///< Enable mask
		RESET_TIME_MASK = (1 << RESET_TIME),			///< Time measurement reset mask
	};

	/**
	 * Memory table class. It is assumed that all the accesses to the
	 * registers will be through the methods and not directly.
	 */
	class MemoryTable {
	private:
		uint8_t data[MEMORY_TABLE_SIZE];			///< Reserved memory for the table

	protected:
	public:
		/**
		 * Default constructor
		 */
		MemoryTable();

		/**
		 * Writes zero (0) to all memory table
		 */
		void clean();

		/**
		 * This function initializes the memory table with default values to the registers.
		 * It must be called before starting operation.
		 */
		void begin();

		/**
		 * @brief Write a byte (unsigned integer of 8 bits) to a register of the memory table
		 *
		 * @param reg is the register to write
		 * @param value is the value to be written
		 *
		 * @see Register
		 */
		void write_byte(Register reg, uint8_t value);

		/**
		 * @brief Read a byte (unsigned integer of 8 bits) from a register of the memory table
		 * using the register enum memory_table::Register
		 *
		 * @note This function is type safe and therefore should be used
		 *
		 * @param reg is the memory_table::Register to be read
		 * @return the register stored value
		 *
		 * Example:
		 * @code
		 * uint8_t input_pull_config;
		 * input_pull_config = read_byte(memory_table::Register::GPIO_INPUT_PULL);
		 * @endcode
		 */
		uint8_t read_byte(Register reg);

		/**
		 * @brief Read a byte (unsigned integer of 8 bits) from a register of the memory table
		 * using the register address (also unsigned integer of 8 bits)
		 *
		 * @param reg is the register address
		 * @return the register stored value
		 */
		uint8_t read_byte(uint8_t reg);

		/**
		 *
		 */
		void write_word(Register reg, uint16_t value);

		/**
		 *
		 */
		uint16_t read_word(Register reg);

		/**
		 *
		 */
		void write_dword(Register reg, uint32_t value);

		/**
		 *
		 */
		void write_float(Register reg, float value);

		/**
		 *
		 */
		float read_float(Register reg);
	};

	/**
	 *
	 */
	extern bool test_read_write(MemoryTable* mt);

}


#ifdef __cplusplus
}
#endif

/**@}*/

#endif /* MANAGER_MEMORY_TABLE_H_ */
