#ifndef DSP_MOVING_AVERAGE_H_
#define DSP_MOVING_AVERAGE_H_

#include <stdint.h>

namespace dsp {

	class MovingAverage {
	private:
		uint8_t length;
		int32_t values[16];
		int32_t sum;
		uint8_t index;

	public:
		MovingAverage();
		~MovingAverage();

		void begin(uint8_t length = 8);
		void add(int32_t value);
		int32_t get_average();
		int32_t get_sum();
		int32_t* get_values();
		void reset(int32_t value = 0);
	};

} // namespace dsp

#endif /* DSP_MOVING_AVERAGE_H_ */
