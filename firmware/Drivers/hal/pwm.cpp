#include "pwm.h"

void set_duty(TIM_HandleTypeDef* timer, uint32_t channel, float duty)
{
    uint32_t raw_duty = ((uint32_t) (timer->Init.Period * duty));
    __HAL_TIM_SET_COMPARE(timer, channel, raw_duty);
}

void set_duty_ch1(float duty)
{
	set_duty(&(BOARD_PWM_TIMER), PWM_CH1, duty);
}

void set_duty_ch2(float duty)
{
	set_duty(&(BOARD_PWM_TIMER), PWM_CH2, duty);
}

void set_duty_ch3(float duty)
{
	set_duty(&(BOARD_PWM_TIMER), PWM_CH3, duty);
}

void set_duty_ch4(float duty)
{
	set_duty(&(BOARD_PWM_TIMER), PWM_CH4, duty);
}

void set_pwm_frequency(TIM_HandleTypeDef* timer, float new_freq)
{
    uint32_t freq_raw = (uint32_t) new_freq;
    int32_t new_period;
    if ((SystemCoreClock / freq_raw) > 0xFFFF)
        timer->Init.Prescaler = 1000;
    else
        timer->Init.Prescaler = 0;

    new_period = ((SystemCoreClock / (timer->Init.Prescaler + 1)) / freq_raw) - 1;
    timer->Init.Period = new_period;
    HAL_TIM_PWM_Init(timer);
}


