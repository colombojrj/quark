#include "isr_functions.h"

/**
  * @brief This function handles EXTI line4 interrupt.
  */
void EXTI4_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4); // ENC1_CHA
}

/**
 * @brief This function handles EXTI line5 interrupt.
 */
void EXTI9_5_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5); // ENC1_CHB
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8); // GPIO_IN_CH1
}

/**
  * @brief This function handles EXTI line[15:10] interrupts.
  */
void EXTI15_10_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10); // ENC2_CHA
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11); // ENC2_CHB
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15); // GPIO_IN_CH2
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14); // GPIO_IN_CH3
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13); // GPIO_IN_CH4
}


