#include "main_cpp.h"

#include <string.h>

#include "Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal.h"
#include "usbd_cdc_if.h"
#include "adc.h"
#include "tim.h"

#include "Config/board.h"
#include "Drivers/hal/pwm.h"
#include "objects.h"


uint32_t adc_buffer_raw[NUMBER_OF_ADS];
RingBuffer<uint8_t, 512> usb_buffer;

void ISR_USB(uint8_t* buffer, uint32_t length)
{
	uint32_t i;
	for (i = 0; i < length; i++)
	    usb_buffer.add(buffer[i]);
}

// Time base (1 ms)
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
    // Trigger an Analog Conversion
    if (manager.is_adc_enabled())
    {
        HAL_ADC_Start_IT(&hadc1);
    }

    // Update the encoders time measurement
    uint8_t i;
    for (i = 0; i < NUMBER_OF_ENCODERS; i++)
    {
        if (encs[i].is_enabled())
        {
            encs[i].isr_timer_overflow();
        }
    }

}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
    if (hadc->Instance == ADC1)
    {
    	manager.adc_read(adc_buffer_raw);
    }
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	// Read how much time has been elapsed
	int32_t counting = (int32_t) __HAL_TIM_GET_COUNTER(&ENC_TIMER);

	switch (GPIO_Pin)
	{
    // Encoder 1
    case ENC1_CHA_Pin:
    case ENC1_CHB_Pin:
        encs[ENCODER_1].isr_pin_change(counting);
        break;

    // Encoder 2
    case ENC2_CHA_Pin:
    case ENC2_CHB_Pin:
        encs[ENCODER_2].isr_pin_change(counting);
        break;

    // GPIO_IN_CHx
    case GPIO_IN_CH1_Pin:
    case GPIO_IN_CH2_Pin:
    case GPIO_IN_CH3_Pin:
    case GPIO_IN_CH4_Pin:
        manager.gpio_read();
        break;
	}
}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
//	if (hi2c->Devaddress == mpu6050_ad0_0.address)
//	{
//		mpu6050_handle_RX_IT_Cplt(&mpu6050_ad0_0, hi2c->Memaddress, hi2c->MemaddSize);
//		readings++;
//	}
}

void main_cpp()
{
    // TODO initialize the GPIO_IN_CHx with interrupts

	// Initialize the manager
	manager.set_memory_table_address(&sram_table);
	manager.set_usb_messenger_address(&usb_msgr);
	manager.set_encoders_address(encs);
	manager.begin_gpio();

	// Start the ADC in DMA mode
	HAL_ADC_Start_DMA(&hadc1, adc_buffer_raw, NUMBER_OF_ADS);

	// Start timer 3 PWM generation
	HAL_TIM_PWM_Start(&(BOARD_PWM_TIMER), PWM_CH1);
	HAL_TIM_PWM_Start(&(BOARD_PWM_TIMER), PWM_CH2);
	HAL_TIM_PWM_Start(&(BOARD_PWM_TIMER), PWM_CH3);
	HAL_TIM_PWM_Start(&(BOARD_PWM_TIMER), PWM_CH4);
	set_duty_ch1(0);
	set_duty_ch2(0);
	set_duty_ch3(0);
	set_duty_ch4(0);

	// Start timer 1 to interrupt every 1 ms
	HAL_TIM_Base_Init(&htim1);
	HAL_TIM_Base_Start_IT(&htim1);

	// Auxiliary variable for indexing
	uint8_t i, usb_aux;
	bool processed_event;

	while (1)
	{
	    if (!usb_buffer.isEmpty())
	    {
	        while (!usb_buffer.isEmpty())
	        {
                usb_buffer.pull(&usb_aux);
                usb_msgr.isr_received_new_byte(usb_aux);
	        }
	    }


        for (i = ENCODER_1; i < NUMBER_OF_ENCODERS; i++)
        {
            processed_event = encs[i].callback();
            if (processed_event == true)
                manager.update_encoder_readings_in_mt(i);
        }


		if (usb_msgr.new_message)
		{
			if (usb_msgr.instruction_packet.is_for_me() == true)
				manager.process_instruction_packet_for_me();

			else if (usb_msgr.instruction_packet.is_for_broadcast() == true)
				manager.process_instruction_packet_for_broadcast();

			usb_msgr.instruction_packet_was_processed();
		}
	}
}


