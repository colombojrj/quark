#ifndef INC_OBJECTS_H_
#define INC_OBJECTS_H_


#include "Config/board.h"
#include "Drivers/manager/memory_table.h"
#include "Drivers/communication/messenger.h"
#include "Drivers/communication/protocol.h"
#include "Drivers/manager/manager.h"
#include "Drivers/encoder/encoder.h"

extern memory_table::MemoryTable sram_table;
extern messenger::Messenger usb_msgr;
extern encoder::Encoder encs[NUMBER_OF_ENCODERS];
extern Manager manager;


#endif /* INC_OBJECTS_H_ */
