#ifndef TIME_H
#define TIME_H

#include "types.h"

/*
 * This code is based on this topic:
 * https://stackoverflow.com/questions/31255486/c-how-do-i-convert-a-stdchronotime-point-to-long-and-back
 */

#include <chrono>
using namespace std::chrono;
const auto _tstart = high_resolution_clock::now();
const u64 _tstart_ms = duration_cast<milliseconds>(time_point_cast<milliseconds>(_tstart).time_since_epoch()).count();
const u64 _tstart_us = duration_cast<microseconds>(time_point_cast<microseconds>(_tstart).time_since_epoch()).count();

/**
 * Get current time (in milliseconds) since system initialization.
 *
 * @return current time in milliseconds.
 */
u32 get_time_ms();

/**
 * Get current time (in microseconds) since system initialization.
 *
 * @return current time in microseconds.
 */
u32 get_time_us();

/**
 * Blocks the CPU during time_to_wait milliseconds.
 *
 * @param time_to_wait
 */
void delay_ms(int time_to_wait);

/**
 * Blocks the CPU during time_to_wait microseconds.
 *
 * @param time_to_wait
 */
void delay_us(int time_to_wait);

#endif //TIME_H
