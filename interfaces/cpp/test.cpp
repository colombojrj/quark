#include "quark.h"
#include <stdio.h>

int main()
{
//    Quark board;
//    board.begin((char*) "/dev/ttyACM0");

    Quark board((char*) "/dev/ttyACM0");

    u32 ti, tf;

    board.encoder_1_set_quadrature(ENCODER_QUADRATURE_4X);
    board.encoder_1_enable();

    board.set_gpio_output_mode(CH3, PUSH_PULL);
    double time_elapsed;
    double freq;

    int i;
    for (i = 0; i < 200; i++)
    {
        ti = get_time_us();
        board.digital_write(CH3, HIGH);
        tf = get_time_us();
        printf("digital_write(3, HIGH) took %d us\n", tf - ti);

        delay_ms(100);

        ti = get_time_us();
        board.digital_write(CH3, LOW);
        tf = get_time_us();
        printf("digital_write(3, LOW)  took %d us\n", tf - ti);

        delay_ms(100);

//        time_elapsed = board.encoder_1_read_time(false);
//        freq = 1.0f / static_cast<double>(time_elapsed);
//        printf("Frequency: %f Hz\n", freq);
//        delay_ms(10);
    }

    return 0;
}
