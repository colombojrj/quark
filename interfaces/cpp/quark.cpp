#include "quark.h"

Quark::Quark()
{
}

Quark::Quark(char* port_name)
{
    begin(port_name);
}

Quark::~Quark()
{
}

void Quark::begin(char* port_name)
{
    // Allocate memory to the buffers, the messages and the receiver
    buffer = new u8[MEMORY_TABLE_SIZE];
    instruction_packet = new dxl::InstructionPacket(MEMORY_TABLE_SIZE + 10);
    status_packet = new dxl::StatusPacket(MEMORY_TABLE_SIZE + 10);
    receiver = new dxl::Receiver(status_packet);
//    timeout_ms = 2;

    // Initialize the registers of the device
    GPIO_INPUT_STATE       = new Register<u8>    (  0, 0x0F, buffer);
    GPIO_INPUT_PULL        = new Register<u8>    (  1, 0x55, buffer);
    GPIO_OUTPUT_STATE      = new Register<u8>    (  2, 0x00, buffer);
    GPIO_OUTPUT_PULL       = new Register<u8>    (  3, 0x00, buffer);
    GPIO_OUTPUT_MODE       = new Register<u8>    (  4, 0x00, buffer);
    ADC_CONFIG             = new Register<u8>    (  5, 0x00, buffer);
    ADC_FILTER_FREQ        = new Register<u16>   (  6, 0x00, buffer);
    ADC_DATA_CH1           = new Register<u16>   (  8, 0x00, buffer);
    ADC_DATA_CH2           = new Register<u16>   ( 10, 0x00, buffer);
    ADC_DATA_CH3           = new Register<u16>   ( 12, 0x00, buffer);
    ADC_DATA_CH4           = new Register<u16>   ( 14, 0x00, buffer);
    PWM_FREQUENCY          = new Register<float> ( 16,  1e4, buffer);
    PWM_DUTY_CH1           = new Register<float> ( 20, 0x00, buffer);
    PWM_DUTY_CH2           = new Register<float> ( 24, 0x00, buffer);
    PWM_DUTY_CH3           = new Register<float> ( 28, 0x00, buffer);
    PWM_DUTY_CH4           = new Register<float> ( 32, 0x00, buffer);
    ENCODER_1_COUNT        = new Register<s32>   ( 36, 0x00, buffer);
    ENCODER_1_TIME         = new Register<u32>   ( 40,  1e5, buffer);
    ENCODER_2_COUNT        = new Register<s32>   ( 44, 0x00, buffer);
    ENCODER_2_TIME         = new Register<u32>   ( 48,  1e5, buffer);
    ENCODER_1_CONFIG       = new Register<u8>    ( 52, 0x04, buffer);
    ENCODER_2_CONFIG       = new Register<u8>    ( 53, 0x04, buffer);
    ENCODER_1_TIMEOUT_MS   = new Register<u8>    ( 54,  100, buffer);
    ENCODER_2_TIMEOUT_MS   = new Register<u8>    ( 55,  100, buffer);
    ENCODER_1_FILTER_SIZE  = new Register<u8>    ( 56,  100, buffer);
    ENCODER_2_FILTER_SIZE  = new Register<u8>    ( 57,  100, buffer);
    ENCODER_1_REL_MAX_RATE = new Register<u8>    ( 58,  100, buffer);
    ENCODER_2_REL_MAX_RATE = new Register<u8>    ( 59,  100, buffer);
    VERSION_MAJOR          = new Register<u8>    (124, 0x00, buffer);
    VERSION_MINOR          = new Register<u8>    (125, 0x00, buffer);
    VERSION_PATCH          = new Register<u8>    (126, 0x00, buffer);
    WHO_AM_I               = new Register<u8>    (127, 0x00, buffer);

    uart.setPortName(QString(port_name));
    uart.open(QSerialPort::ReadWrite);

    if (!uart.isOpen())
    {
        printf("(ERROR) The serial port is NOT OPEN\n");
    }

    else
    {
        #if QUARK_DEBUG_LEVEL > 0
        printf("(DEBUG) The serial port is OPEN\n");
        #endif
    }
}

void Quark::read_all()
{
    read_bytes(0, 64);
}

void Quark::set_gpio_input_pull(int channel, int pull)
{
    u8 mask = 0, pos = 0;

    if ((channel >= 1) && (channel <= 4))
    {
        switch (channel)
        {
            case 1:
                pos = static_cast<u8>(eGPIO_INPUT_PULL::CH1);
                mask = static_cast<u8>(eGPIO_INPUT_PULL::CH1_MASK);
                break;
            case 2:
                pos = static_cast<u8>(eGPIO_INPUT_PULL::CH2);
                mask = static_cast<u8>(eGPIO_INPUT_PULL::CH2_MASK);
                break;
            case 3:
                pos = static_cast<u8>(eGPIO_INPUT_PULL::CH3);
                mask = static_cast<u8>(eGPIO_INPUT_PULL::CH3_MASK);
                break;
            case 4:
                pos = static_cast<u8>(eGPIO_INPUT_PULL::CH4);
                mask = static_cast<u8>(eGPIO_INPUT_PULL::CH4_MASK);
                break;
            default:
                break;
        }

        u8 config = GPIO_INPUT_PULL->read();
        config = config & ~mask;
        config = config | (static_cast<u8>(pull) << pos);
        GPIO_INPUT_PULL->write(config);

        write_reg(GPIO_INPUT_PULL);
        receive_status_packet();
    }
}

void Quark::set_gpio_output_pull(int channel, int pull)
{
    u8 mask = 0, pos = 0;

    if ((channel >= 1) && (channel <= 4))
    {
        switch (channel)
        {
            case 1:
                pos = static_cast<u8>(eGPIO_OUTPUT_PULL::CH1);
                mask = static_cast<u8>(eGPIO_OUTPUT_PULL::CH1_MASK);
                break;
            case 2:
                pos = static_cast<u8>(eGPIO_OUTPUT_PULL::CH2);
                mask = static_cast<u8>(eGPIO_OUTPUT_PULL::CH2_MASK);
                break;
            case 3:
                pos = static_cast<u8>(eGPIO_OUTPUT_PULL::CH3);
                mask = static_cast<u8>(eGPIO_OUTPUT_PULL::CH3_MASK);
                break;
            case 4:
                pos = static_cast<u8>(eGPIO_OUTPUT_PULL::CH4);
                mask = static_cast<u8>(eGPIO_OUTPUT_PULL::CH4_MASK);
                break;
            default:
                break;
        }

        u8 config = GPIO_OUTPUT_PULL->read();
        config = config & ~mask;
        config = config | (static_cast<u8>(pull) << pos);
        GPIO_OUTPUT_PULL->write(config);

        write_reg(GPIO_OUTPUT_PULL);
        receive_status_packet();
    }
}

void Quark::set_gpio_output_mode(int channel, int mode)
{
    u8 mask = 0, pos = 0;

    if ((channel >= 1) && (channel <= 4)) {
        switch (channel) {
            case 1:
                pos = static_cast<u8>(eGPIO_OUTPUT_MODE::CH1);
                mask = static_cast<u8>(eGPIO_OUTPUT_MODE::CH1_MASK);
                break;
            case 2:
                pos = static_cast<u8>(eGPIO_OUTPUT_MODE::CH2);
                mask = static_cast<u8>(eGPIO_OUTPUT_MODE::CH2_MASK);
                break;
            case 3:
                pos = static_cast<u8>(eGPIO_OUTPUT_MODE::CH3);
                mask = static_cast<u8>(eGPIO_OUTPUT_MODE::CH3_MASK);
                break;
            case 4:
                pos = static_cast<u8>(eGPIO_OUTPUT_MODE::CH4);
                mask = static_cast<u8>(eGPIO_OUTPUT_MODE::CH4_MASK);
                break;
            default:
                break;
        }

        u8 config = GPIO_OUTPUT_MODE->read();

        config = config & ~mask;
        config = config | (static_cast<u8>(mode) << pos);
        GPIO_OUTPUT_MODE->write(config);

        write_reg(GPIO_OUTPUT_MODE);
        receive_status_packet();
    }
}

int Quark::digital_read(int channel, bool from_buffer)
{
    int value = -1;
    if ((channel >= 1) && (channel <= 4))
    {
        u8 mask = 0;
        switch (channel) {
            case 1:
                mask = static_cast<u8>(eGPIO_INPUT_STATE::CH1_MASK);
                break;
            case 2:
                mask = static_cast<u8>(eGPIO_INPUT_STATE::CH2_MASK);
                break;
            case 3:
                mask = static_cast<u8>(eGPIO_INPUT_STATE::CH3_MASK);
                break;
            case 4:
                mask = static_cast<u8>(eGPIO_INPUT_STATE::CH4_MASK);
                break;
            default:
                break;
        }

        if (!from_buffer)
            read_reg(GPIO_INPUT_STATE);

        value = (int) ((GPIO_INPUT_STATE->read() & mask) > 0);
    }

    return value;
}

void Quark::digital_write(int channel, int state)
{
    if ((channel >= 1) && (channel <= 4)) {
        u8 pos;

        switch (channel) {
            case 1:
                pos = static_cast<u8>(eGPIO_OUTPUT_STATE::CH1);
                break;
            case 2:
                pos = static_cast<u8>(eGPIO_OUTPUT_STATE::CH2);
                break;
            case 3:
                pos = static_cast<u8>(eGPIO_OUTPUT_STATE::CH3);
                break;
            case 4:
                pos = static_cast<u8>(eGPIO_OUTPUT_STATE::CH4);
                break;
            default:
                break;
        }

        if (state == HIGH)
            GPIO_OUTPUT_STATE->set_bit(pos);
        else
            GPIO_OUTPUT_STATE->clear_bit(pos);

        write_reg(GPIO_OUTPUT_STATE);
        receive_status_packet();
    }
}

double Quark::analog_read(int channel, bool from_buffer)
{
    double value = 0;
    if ((channel >= 1) && (channel <= 4))
    {
        Register<u16>* reg = nullptr;
        switch (channel) {
            case CH1:
                reg = ADC_DATA_CH1;
                break;
            case CH2:
                reg = ADC_DATA_CH2;
                break;
            case CH3:
                reg = ADC_DATA_CH3;
                break;
            case CH4:
                reg = ADC_DATA_CH4;
                break;
            default:
                break;
        }

        if (!from_buffer)
            read_reg(reg);

        value = K_ADC * static_cast<double>(reg->read());
    }

    return value;
}

void Quark::adc_enable()
{
    ADC_CONFIG->set_bit(static_cast<u8>(eADC_CONFIG::ENABLE));
    write_reg(ADC_CONFIG);
    receive_status_packet();
}

void Quark::adc_disable()
{
    ADC_CONFIG->clear_bit(static_cast<u8>(eADC_CONFIG::ENABLE));
    write_reg(ADC_CONFIG);
    receive_status_packet();
}

void Quark::pwm_write(int channel, double duty)
{
    if ((channel >= 1) && (channel <= 4) && (duty >= PWM_MIN_DUTY) && (duty <= PWM_MAX_DUTY)) {
        Register<float> *reg = nullptr;
        switch (channel) {
            case CH1:
                reg = PWM_DUTY_CH1;
                break;
            case CH2:
                reg = PWM_DUTY_CH2;
                break;
            case CH3:
                reg = PWM_DUTY_CH3;
                break;
            case CH4:
                reg = PWM_DUTY_CH4;
                break;
            default:
                break;
        }

        reg->write(duty);
        write_reg(reg);
        receive_status_packet();
    }
}

void Quark::pwm_set_frequency(double freq_in_hz)
{
    if ((freq_in_hz >= PWM_MIN_FREQUENCY) && (freq_in_hz <= PWM_MAX_FREQUENCY))
    {
        PWM_FREQUENCY->write(freq_in_hz);
        write_reg(PWM_FREQUENCY);
        receive_status_packet();
    }
}

double Quark::pwm_get_frequency(bool from_buffer)
{
    if (!from_buffer)
        read_reg(PWM_FREQUENCY);

    return static_cast<double>(PWM_FREQUENCY->read());
}

long int Quark::encoder_1_read_count(bool from_buffer)
{
    if (!from_buffer)
        read_reg(ENCODER_1_COUNT);

    return static_cast<long int>(ENCODER_1_COUNT->read());
}

double Quark::encoder_1_read_time(bool from_buffer)
{
    if (!from_buffer)
        read_reg(ENCODER_1_TIME);

    return static_cast<double>(ENCODER_1_TIME->read()) / 1e6f;
}

long int Quark::encoder_2_read_count(bool from_buffer)
{
    if (!from_buffer)
        read_reg(ENCODER_2_COUNT);

    return static_cast<long int>(ENCODER_2_COUNT->read());
}

double Quark::encoder_2_read_time(bool from_buffer)
{
    if (!from_buffer)
        read_reg(ENCODER_2_TIME);

    return static_cast<double>(ENCODER_2_TIME->read()) / 1e6f;
}

void Quark::encoder_1_set_direction(int positive_direction)
{
    encoder_set_direction(1, positive_direction);
}

void Quark::encoder_2_set_direction(int positive_direction)
{
    encoder_set_direction(2, positive_direction);
}

void Quark::encoder_1_reset_counting()
{
    encoder_reset_counting(1);
}

void Quark::encoder_2_reset_counting()
{
    encoder_reset_counting(2);
}

void Quark::encoder_1_enable()
{
    encoder_enable(1);
}

void Quark::encoder_2_enable()
{
    encoder_enable(2);
}

void Quark::encoder_1_disable()
{
    encoder_disable(1);
}

void Quark::encoder_2_disable()
{
    encoder_disable(2);
}

void Quark::encoder_1_set_quadrature(int quadrature)
{
    encoder_set_quadrature(1, quadrature);
}

void Quark::encoder_2_set_quadrature(int quadrature)
{
    encoder_set_quadrature(2, quadrature);
}

void Quark::encoder_1_set_timeout(unsigned int msecs)
{
    encoder_set_timeout(1, msecs);
}

void Quark::encoder_2_set_timeout(unsigned int msecs)
{
    encoder_set_timeout(2, msecs);
}

void Quark::encoder_1_set_filter_size(unsigned int size)
{
    encoder_set_filter_size(1, size);
}

void Quark::encoder_2_set_filter_size(unsigned int size)
{
    encoder_set_filter_size(2, size);
}

void Quark::encoder_1_set_rel_max_rate(unsigned int max_rate)
{
    encoder_set_rel_max_rate(1, max_rate);
}

void Quark::encoder_2_set_rel_max_rate(unsigned int max_rate)
{
    encoder_set_rel_max_rate(2, max_rate);
}

int Quark::get_firmware_major_version(bool from_buffer)
{
    if (!from_buffer)
        read_reg(VERSION_MAJOR);
    return static_cast<u8>(VERSION_MAJOR->read());
}

int Quark::get_firmware_minor_version(bool from_buffer)
{
    if (!from_buffer)
        read_reg(VERSION_MINOR);
    return static_cast<u8>(VERSION_MINOR->read());
}

int Quark::get_firmware_patch_version(bool from_buffer)
{
    if (!from_buffer)
        read_reg(VERSION_PATCH);
    return static_cast<u8>(VERSION_PATCH->read());
}

void Quark::encoder_reset_counting(int what_encoder)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_CONFIG;
        if (what_encoder == 2)
            reg = ENCODER_2_CONFIG;

        reg->set_bit(static_cast<u8>(eENCODER_CONFIG::RESET_COUNTING));

        write_reg(reg);
        receive_status_packet();
    }
}

void Quark::encoder_enable(int what_encoder)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_CONFIG;
        if (what_encoder == 2)
            reg = ENCODER_2_CONFIG;

        reg->set_bit(static_cast<u8>(eENCODER_CONFIG::ENABLE));

        write_reg(reg);
        receive_status_packet();
    }
}

void Quark::encoder_disable(int what_encoder)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_CONFIG;
        if (what_encoder == 2)
            reg = ENCODER_2_CONFIG;

        reg->clear_bit(static_cast<u8>(eENCODER_CONFIG::ENABLE));

        write_reg(reg);
        receive_status_packet();
    }
}

void Quark::encoder_set_quadrature(int what_encoder, int quadrature)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_CONFIG;
        if (what_encoder == 2)
            reg = ENCODER_2_CONFIG;

        u8 mask = static_cast<u8>(eENCODER_CONFIG::QUADRATURE_MASK);
        u8 config = reg->read();
        config = config & ~mask;
        config = config | static_cast<u8>(quadrature);
        reg->write(config);
        write_reg(reg);
        receive_status_packet();
    }
}

void Quark::encoder_set_timeout(int what_encoder, unsigned int msecs)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_TIMEOUT_MS;
        if (what_encoder == 2)
            reg = ENCODER_2_TIMEOUT_MS;

        reg->write(static_cast<u8>(msecs));

        write_reg(reg);
        receive_status_packet();
    }
}

void  Quark::encoder_set_filter_size(int what_encoder, unsigned int size)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_FILTER_SIZE;
        if (what_encoder == 2)
            reg = ENCODER_2_FILTER_SIZE;

        reg->write(static_cast<u8>(size));
        write_reg(reg);
        receive_status_packet();
    }
}

void  Quark::encoder_set_rel_max_rate(int what_encoder, unsigned int max_rate)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_REL_MAX_RATE;
        if (what_encoder == 2)
            reg = ENCODER_2_REL_MAX_RATE;

        reg->write(static_cast<u8>(max_rate));
        write_reg(reg);
        receive_status_packet();
    }
}

void Quark::encoder_set_direction(int what_encoder, int direction)
{
    if ((what_encoder >= 1) && (what_encoder <= 2))
    {
        Register<u8>* reg = ENCODER_1_CONFIG;
        if (what_encoder == 2)
            reg = ENCODER_2_CONFIG;

        if (direction == ENCODER_CW_IS_POSITIVE)
            reg->set_bit(static_cast<u8>(eENCODER_CONFIG::DIRECTION));
        else
            reg->clear_bit(static_cast<u8>(eENCODER_CONFIG::DIRECTION));

        write_reg(reg);
        receive_status_packet();
    }
}


int Quark::get_who_am_i()
{
    read_bytes(WHO_AM_I->get_addr(), WHO_AM_I->get_total_size());
    return static_cast<int>(WHO_AM_I->read());
}

void Quark::close()
{
    if (uart.isOpen())
        uart.close();
}

void Quark::receive_raw_data()
{
    u8 new_byte[256];
    int number_of_bytes_read;
    int number_of_available_bytes = 1;

    while ((number_of_available_bytes > 0) && (!receiver->new_message))
    {
        uart.waitForReadyRead(timeout_ms);
        number_of_available_bytes = uart.bytesAvailable();
        number_of_bytes_read = uart.read((char*) new_byte, number_of_available_bytes);
        if (number_of_bytes_read > 0)
            receiver->received_new_data(new_byte, number_of_bytes_read);
    }

    if ((receiver->is_valid) && (status_packet->get_id() == QUARK_ID))
        decode_status_packet();
}

void Quark::receive_raw_data(u8* data, u16 size)
{
    receiver->received_new_data(data, size);
}

void Quark::send_raw_data(u8* data, u16 size)
{
    uart.write((char*) data, size);
    uart.waitForBytesWritten(timeout_ms);
}

void Quark::read_bytes(u8 first_register, u16 how_many)
{
    // Make sure the receiver is in reset state
    receiver->clean();

    // Build a new instruction packet
    instruction_packet->create_new_msg(QUARK_ID, dxl::Command::READ);
    instruction_packet->append_parameter(first_register);
    instruction_packet->append_parameter(how_many);
    instruction_packet->update_checksum_value();

    // Send the instruction packet
    u8* data = instruction_packet->get_addr_of_packet();
    u16 size = instruction_packet->get_total_packet_size();
    send_raw_data(data, size);
    receive_status_packet();
}

void Quark::write_bytes(u8 first_register, u8 *data, u16 how_many)
{
    // Make sure the receiver is in reset state
    receiver->clean();

    // Build a new instruction packet
    instruction_packet->create_new_msg(QUARK_ID, dxl::Command::WRITE);
    instruction_packet->append_parameter(first_register);
    instruction_packet->append_parameters(data, static_cast<u8>(how_many));
    instruction_packet->update_checksum_value();

    // Send the instruction packet
    u8* packet_ptr = instruction_packet->get_addr_of_packet();
    u16 size = instruction_packet->get_total_packet_size();
    send_raw_data(packet_ptr, size);
}

void Quark::receive_status_packet()
{
    status_packet->clean_message();
    receive_raw_data();
}

void Quark::decode_read_command()
{
    u16 first_register = instruction_packet->get_parameter(0);
    u16 size = instruction_packet->get_parameter(1);
    u8* parameters = status_packet->get_addr_of_parameters();

    u16 i;
    for (i = 0; i < size; i++)
        buffer[first_register + i] = parameters[i];
}

void Quark::decode_status_packet()
{
    if (instruction_packet->get_command() == dxl::Command::WRITE)
    {
        if (status_packet->is_valid())
        {
            // If it was a write operation, then we have the write result to decode
//            u16 first_address = instruction_packet->get_parameter(0);
//            u16 final_address = first_address + instruction_packet->get_number_of_parameters() - 1;
//            if (first_address == PIPE0_DATA->get_addr(0) && (WRITE_AS->get_addr() < final_address))
//            {
//                STATUS->write(status_packet->get_parameter(0));
//            }
        }
    }

    else if (instruction_packet->get_command() == dxl::Command::READ)
    {
        if (status_packet->is_valid())
        {
            decode_read_command();
        }
    }
}
