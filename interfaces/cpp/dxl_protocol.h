#ifndef DXL_PROTOCOL_H
#define DXL_PROTOCOL_H

/**
  ******************************************************************************
  * @file    dxl_protocol.h
  * @author  José Roberto Colombo Junior
  * @brief   Header file of the DXL 1.0 protocol implementation
  ******************************************************************************
  * @attention
  *
  * This is free software licensed under GPL.
  *
  ******************************************************************************
  */

#include "utils/types.h"
#include "utils/time.h"

/**
 * @defgroup communication Communication
 *
 * The board is controlled from the computer. The communication protocol is
 * based on the DXL Protocol 1.0.
 *
 */

/**@{*/

/**
 * @brief Defines the DXL Protocol namespace
 */
namespace dxl
{
    /**
     * Broadcast ID defined in this protocol.
     */
    constexpr u8 BROADCAST_ID = 0xFE;

    /**
     * @brief Field positions of the message
     */
    enum class Field : u8 {
        HEADER_1 = 0,                           ///< Header 1 field position
        HEADER_2 = 1,                           ///< Header 2 field position
        ID = 2,                                 ///< ID field position
        LENGTH = 3,                             ///< Length field position
        COMMAND_OR_ERROR = 4,                   ///< Command or error field position
        FIRST_PARAMETER = 5,                    ///< First parameter field position
    };

    // Default values of the fields
    constexpr u8 HEADER_1_VALUE = 0xFF;         ///< Default HEADER 1 value
    constexpr u8 HEADER_2_VALUE = 0xFF;         ///< Default HEADER 2 value

    /**
     * @brief Types of commands
     */
    enum class Command : u8 {
        PING = 0x01,                            ///< PING command (verify if the device is working)
        READ = 0x02,
        WRITE = 0x03,
        REG_WRITE = 0x04,
        ACTION = 0x05,
        FACTORY_RESET = 0x06,
        REBOOT = 0x08,
        SYNC_WRITE = 0x83,
        BULK_READ = 0x92,
    };

    // Types of errors
    enum class Error : u8 {
        INVALID_INSTRUCTION = 1 << 6,
        OVERLOAD_ERROR = 1 << 5,
        INVALID_CHECKSUM = 1 << 4,
        RANGE_ERROR = 1 << 3,
        OVERHEATING_ERROR = 1 << 2,
        ANGLE_LIMIT_ERROR = 1 << 1,
        INPUT_VOLTAGE_ERROR = 1 << 0,
    };

    class Message {
    private:
    protected:
        u8* data;                              ///< Reserved RAM for message data
        const u16 data_size;                   ///< Total size reserved for message data
        u8* header_1;                          ///< This variable points to header 1 field position
        u8* header_2;                          ///< This variable points to header 2 field position
        u8* id;                                ///< This variable points to id field position
        u8* length;                            ///< This variable points to length field position
        u8* command;                           ///< This variable points to command field 1 position
        u8* error;                             ///< This variable points to error field position
        u8* parameters;                        ///< Pointer to parameters field
        u8* checksum;                          ///< Pointer to checksum field

    public:
        // Constructor
        Message(u16 memory_size);

        // Destructor
        virtual ~Message();

        /**
         * @brief Sets the given value to the header 1 field
         *
         * @param new_value is the the value to be written in the header 1 field
         */
        void set_header_1(u8 new_value);
        void set_header_2(u8 new_value);
        void set_id(u8 new_value);
        void set_command(u8 new_value);
        void set_length(u8 new_value);
        void append_parameter(u8 value);
        void append_parameters(u8* values, u8 size);
        void insert_parameter(u16 index, u8 value);
        void set_checksum(u8 new_value);
        void update_checksum_value();

        u8 get_parameter(u16 what_parameter);
        u8* get_addr_of_parameters();
        u8 get_number_of_parameters();
        u8 get_id();
        u8 get_length();
        u8 get_checksum();
        u8* get_addr_of_packet();
        u16 get_total_packet_size();

        void clean_message();

        void update_checksum_position();
        u8 calculate_checksum();
        bool is_checksum_valid();
        bool is_for_me(u8 my_id);
        bool is_for_broadcast();
        virtual bool is_valid();
    };

    class InstructionPacket : public Message
    {
    private:
    public:
        // Constructor
        InstructionPacket(u16 memory_size);

        // Destructor
        ~InstructionPacket();


        /**
         * @brief Verify if the received command is valid
         *
         * Only the supported commands will be considered valid.
         */
        bool is_command_valid();
        bool is_this_command_valid(u8 command);
        Command get_command();
        void create_new_msg(u8 to_id, Command cmd);

        bool is_valid();
    };

    class StatusPacket : public Message
    {
    private:
    public:
        // Constructor
        StatusPacket(u16 memory_size);

        ~StatusPacket();

        bool is_valid();

        void create_new_msg(u8 from_id);

        void append_error(Error what_error);
        void clear_error(Error what_error);
        void clear_errors();
    };

    /**
     * This is a helper class to receive messages. The method received_new_data
     * must be called every time new data arrives. If the received bytes represent
     * a valid message, then the attribute new_message will be true.
     *
     * After processing the received message, the method clean must be called.
     */
    class Receiver {
    protected:
        /**
         * This enum models the state of the Receiver. It is used internally.
         */
        enum class State: u8 {
            LOOKING_FOR_HEADER_1 = 0,               ///< No valid data arrived.
            LOOKING_FOR_HEADER_2,                   ///< Found HEADER 1 and now it is looking for HEADER 2.
            LOOKING_FOR_ID,                         ///< Found HEADER 2 and now it is looking for ID.
            LOOKING_FOR_LENGTH,                     ///< Found ID and now it is looking for LENGTH.
            LOOKING_FOR_CMD_OR_ERROR,               ///< Found LENGTH and now it is looking for COMMAND or ERROR.
            RECEIVING_PARAMETERS,                   ///< Found COMMAND or ERROR and now it is receiving parameters.
            LOOKING_FOR_CHECKSUM,                   ///< Finished receiving parameters and it is looking for CHECKSUM.
        };

        State state;                                ///< The intern state of the receiver.

        u32 timeout_us = 2000;
        u32 time_received_header_1;
        u16 n_parameters_to_receive;

        Message* msg;                               ///< Pointer to where save the new data.

        void process_new_byte(u8 new_byte);

    public:
        /**
         * Constructor of the receiver. It is required to pass a pointer to an instance
         * of a message. The received data will be put in that message.
         *
         * @param msg_ptr is a pointer to a message.
         *
         * @see dxl_protocol::Message
         */
        Receiver(Message* msg_ptr);

        /**
         * Destructor.
         */
        ~Receiver();

        /**
         * This variable is true when a new message arrived.
         */
        bool new_message;

        /**
         * This variable is true if the received message is valid.
         */
        bool is_valid;

        /**
         * This variable contains the transmission errors.
         *
         * @see dxl_protocol::Error
         */
        uint8_t errors;

        /**
         * This function must be called when new data arrives.
         *
         * @param [in] data is a pointer to the new data
         * @param [in] size is how many bytes arrived
         */
        void received_new_data(u8* data, u16 size);

        /**
         * This function is used to clean all intern variables and must be called before
         * a new message starts to be processed.
         */
        void clean();

    };

} // namespace dxl_protocol

/**@}*/

#endif //DXL_PROTOCOL_H
