class Register:
    def __init__(self, name, address, ctype, endian='little'):
        self.name = name
        self.address = address
        self.ctype = ctype

        self.endian = '<' if endian == 'little' else '>'

        self.__value = None
        self.__get_struct_code()

    def __get_struct_code(self):
        self.size = None
        self.code = None
        if self.ctype == 'uint8_t':
            self.size = 1
            self.code = '%sB' % self.endian

        elif self.ctype == 'int8_t':
            self.size = 1
            self.code = '%sb' % self.endian

        elif self.ctype == 'uint16_t':
            self.size = 2
            self.code = '%sH' % self.endian

        elif self.ctype == 'int16_t':
            self.size = 2
            self.code = '%sh' % self.endian

        elif self.ctype == 'uint32_t':
            self.size = 4
            self.code = '%sI' % self.endian

        elif self.ctype == 'int32_t':
            self.size = 4
            self.code = '%si' % self.endian

        elif self.ctype == 'float':
            self.size = 4
            self.code = '%sf' % self.endian

    def next_free_adress(self):
        return self.address + self.size

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, new_value):
        self.__value = new_value

    def used_addresses(self):
        return [hex(item) for item in list(range(self.address, self.address + self.size))]

    def __repr__(self):
        return 'Register: %s' % self.name


MemoryTable = {
    'GPIO_INPUT_STATE': Register('GPIO_INPUT_STATE', 0x00, 'uint8_t'),
    'GPIO_INPUT_PULL': Register('GPIO_INPUT_PULL', 0x01, 'uint8_t'),
    'GPIO_OUTPUT_STATE': Register('GPIO_OUTPUT_STATE', 0x02, 'uint8_t'),
    'GPIO_OUTPUT_PULL': Register('GPIO_OUTPUT_PULL', 0x03, 'uint8_t'),
    'GPIO_OUTPUT_MODE': Register('GPIO_OUTPUT_MODE', 0x04, 'uint8_t'),
    'ADC_CONFIG': Register('ADC_CONFIG', 0x05, 'uint8_t'),
    'ADC_FILTER_FREQ': Register('ADC_FILTER_FREQ', 0x06, 'uint16_t'),
    'ADC_DATA_CH1': Register('ADC_DATA_CH1', 0x08, 'uint16_t'),
    'ADC_DATA_CH2': Register('ADC_DATA_CH2', 0x0A, 'uint16_t'),
    'ADC_DATA_CH3': Register('ADC_DATA_CH3', 0x0C, 'uint16_t'),
    'ADC_DATA_CH4': Register('ADC_DATA_CH4', 0x0E, 'uint16_t'),
    'PWM_FREQUENCY': Register('PWM_FREQUENCY', 0x10, 'float'),
    'PWM_DUTY_CH1': Register('PWM_DUTY_CH1', 0x14, 'float'),
    'PWM_DUTY_CH2': Register('PWM_DUTY_CH2', 0x18, 'float'),
    'PWM_DUTY_CH3': Register('PWM_DUTY_CH3', 0x1C, 'float'),
    'PWM_DUTY_CH4': Register('PWM_DUTY_CH4', 0x20, 'float'),
    'ENCODER_1_COUNT': Register('ENCODER_1_COUNT', 0x24, 'int32_t'),
    'ENCODER_1_TIME': Register('ENCODER_1_TIME', 0x28, 'uint32_t'),
    'ENCODER_2_COUNT': Register('ENCODER_2_COUNT', 0x2C, 'int32_t'),
    'ENCODER_2_TIME': Register('ENCODER_2_TIME', 0x30, 'uint32_t'),
    'ENCODER_1_CONFIG': Register('ENCODER_1_CONFIG', 0x34, 'uint8_t'),
    'ENCODER_2_CONFIG': Register('ENCODER_2_CONFIG', 0x35, 'uint8_t'),
    'ENCODER_1_TIMEOUT_MS': Register('ENCODER_1_TIMEOUT_MS', 0x36, 'uint8_t'),
    'ENCODER_2_TIMEOUT_MS': Register('ENCODER_1_TIMEOUT_MS', 0x37, 'uint8_t'),
    'VERSION_MAJOR': Register('VERSION_MAJOR', 0x7C, 'uint8_t'),
    'VERSION_MINOR': Register('VERSION_MINOR', 0x7D, 'uint8_t'),
    'VERSION_PATCH': Register('VERSION_PATCH', 0x7E, 'uint8_t'),
    'WHO_AM_I': Register('WHO_AM_I', 0x7F, 'uint8_t'),
}


def verify_consistency():
    regs = list(MemoryTable.values())
    for reg1 in regs:
        for reg2 in regs:
            if reg1 is not reg2:
                reg1_addresses = set(reg1.used_addresses())
                reg2_addresses = set(reg2.used_addresses())

                if len(reg1_addresses.intersection(reg2_addresses)) > 0:
                    print('Collision between %s and %s' % (reg1.name, reg2.name))


if __name__ == '__main__':
    verify_consistency()


